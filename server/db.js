const mongoose     = require('mongoose')
const {Schema}     = mongoose
const {ObjectId}   = Schema.Types

const schemas = {
  user: new Schema({
    nome: String,
  }),
  image: new Schema({
    url: String,
    upvotes: Number,
    downvotes: Number,
    payments: Number,
    author: String,
    pointer: String,
  }),
}

const User           = mongoose.model('user', schemas.user)
const Image          = mongoose.model('image', schemas.image)
module.exports       = {
  User, Image,
  init() {
    return new Promise((resolve, reject) => {
      const options = {
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true
      }
      return mongoose.connect(`mongodb://localhost/kult`, options, err =>
        err ? reject(err) : resolve()
      )
    })
  }
}
