const {User,Image,init}  = require('./db.js')
const {createServer}     = require('http')

function readBody(request) {
  return new Promise((resolve, reject) => {
    const body = []
    request.on('data', chunk => body.push(chunk))
    request.on('end', () =>
      resolve(JSON.parse(Buffer.concat(body).toString()))
    )
    request.on('error', reject)
  })
}

Promise.resolve()
.then(init)
.then(() => {
  const server = createServer(async (request, response) => {
    response.setHeader('Content-Type', 'application/json')
    response.setHeader('Access-Control-Allow-Origin', '*')
    response.setHeader('Access-Control-Allow-Headers', 'POST, PUT, GET, OPTIONS, content-type')
    try {
      if (request.url === '/imagens') {
        let images = await Image.find({})
        images = Array.from(images)
        images.reverse()
        return response.end(JSON.stringify(images))
      }
      if (request.url === '/imagem') {
        if (request.method === 'POST') {
          const imagem = await readBody(request)
          const image = await Image.create(imagem)
          return response.end(JSON.stringify(image))
        }
      }
      if (request.url === '/update-imagem') {
        if (request.method === 'POST') {
          const imagem = await readBody(request)
          const image = await Image.findById(imagem._id)
          Object.assign(image, imagem)
          await image.save()
          return response.end(JSON.stringify({success: true}))
        }
      }
      response.end(JSON.stringify({error: 'Não encontrado'}))
    } catch (e) {
      console.log(e)
      return response.end(JSON.stringify({error: e}))
    }
  })
  server.listen(8081, err => {
    if (err) return console.log('erro: ', err)
    console.log('Ouvindo em 8081')
  })
})
