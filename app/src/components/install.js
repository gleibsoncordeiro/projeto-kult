import menu from './menu.vue'
import imagem from './imagem.vue'
import icon from './icon.vue'

export default function install(Vue) {
  Vue.component('c-menu', menu)
  Vue.component('c-imagem', imagem)
  Vue.component('c-icon', icon)
}
