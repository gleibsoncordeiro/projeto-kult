import './wm.js'

import { createApp } from 'vue'
import App from './App.vue'
import router from './router.js'
import store from './store.js'

import components from './components/install.js'

createApp(App)
.use(store)
.use(router)
.use(components)
.mount('#app')
