import { createStore } from 'vuex'
import {getImages} from '@/api.js'
import {sendImage} from '@/api.js'
import {updateImage} from '@/api.js'

export default createStore({
  state: {
    images: [],
  },
  getters: {
    images(state) {
      return state.images
    },
  },
  mutations: {
    async create(state, imagem) {
      state.images.splice(0,0,imagem)
    },
    images(state, images) {
      state.images = images
    },
  },
  actions: {
    getImages({commit}) {
      getImages()
      .then(images => commit('images', images))
    },
    async create({commit}, url) {
      const imagem = await sendImage({
        url: url,
        upvotes: 0,
        downvotes: 0,
        payments: 0,
        author: 'Usuário',
        pointer: '$wallet.example.com/test',
      })
      commit('create', imagem)
    },
    async upvote({state}, id) {
      const image = state.images.find(x => x._id == id)
      if (image) image.upvotes += 1
      await updateImage(image)
    },
    async downvote({state}, id) {
      const image = state.images.find(x => x._id == id)
      if (image) image.downvotes += 1
      await updateImage(image)
    },
    async pay({state}, {id, value}) {
      const image = state.images.find(x => x._id == id)
      if (image) image.payments += value
      await updateImage(image)
    },
  },
})
