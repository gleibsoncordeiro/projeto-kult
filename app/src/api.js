const ip = '10.0.0.102'

export function getImages() {
  return fetch(`http://${ip}:8081/imagens`)
  .then(res => res.json())
}
export function sendImage(imagem) {
  return fetch(`http://${ip}:8081/imagem`, {method: 'POST', mode: 'cors', headers: {'Content-Type': 'application/json'}, body: JSON.stringify(imagem)})
  .then(res => res.json())
}
export function updateImage(imagem) {
  return fetch(`http://${ip}:8081/update-imagem`, {method: 'POST', mode: 'cors', headers: {'Content-Type': 'application/json'}, body: JSON.stringify(imagem)})
  .then(res => res.json())
}
