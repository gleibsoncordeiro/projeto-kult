// from https://webmonetization.org/docs/counter/

/* eslint-disable */
function getWmTag () {
  return document.head.querySelector('meta[name="monetization"]')
}

function uuid () {
  // TODO: make the uuid random
  return 'fc23b14d-70e4-4d55-b0a0-dd86f70ce402'
}

function wmStateTransition () {
  const wmTag = getWmTag()
  const state = document.monetization.state
  if (wmTag) {
    if (state === 'stopped') {
      startWm()
    }
  }
}

function observeWm () {
  const headObserver = new MutationObserver(mutations => {
    const wmTag = getWmTag()
    const state = document.monetization.state

    if (!wmTag && state === 'started') stopWm()
    if (wmTag && state === 'stopped') {
      pendingWm()
      // TODO: observe wmTag for attribute changes?
    }
  })
  
  headObserver.observe(document.head, { childList: true })
}

function initWm () {
  document.monetization = document.createElement('div')
  document.monetization.state = 'stopped'

  observeWm()
  if (getWmTag()) {
    pendingWm()
  }
}

function pendingWm () {
  document.monetization._requestId = uuid()
  emitWm('pending', 'pending')
  document.monetization._startWmTimer = setTimeout(startWm, 700)
}

function startWm () {
  emitWm('start', 'started')
  progressWm()
  document.monetization._wmProgressInterval = setInterval(progressWm, 1000)
}

function emitWm (name, state) {
  const wmTag = getWmTag()
  if (!wmTag) {
    throw new Error('cannot emit ' + name + ' w/o meta tag')
  }
  
  document.monetization._wmTag = wmTag
  document.monetization.state = state
  document.monetization.dispatchEvent(new CustomEvent('monetization' + name, {
    detail: {
      requestId: document.monetization._requestId,
      paymentPointer: wmTag.content 
    }
  }))
}

function progressWm () {
  const wmTag = document.monetization._wmTag 
  if (!wmTag) {
    throw new Error('cannot emit progress w/o meta tag')
  }
  
  document.monetization.dispatchEvent(new CustomEvent('monetizationprogress', {
    detail: {
      requestId: document.monetization._requestId,
      paymentPointer: wmTag.content,
      amount: String(Math.floor(50000 + Math.random() * 100000)),
      assetCode: 'USD',
      assetScale: 9
    }
  }))
}

function stopWm () {
  const wmTag = document.monetization._wmTag 
  if (!wmTag) {
    throw new Error('cannot emit stop w/o meta tag')
  }
  
  
  if (document.monetization._startWmTimer) {
    clearTimeout(document.monetization._startWmTimer)
  }
  
  if (document.monetization._wmProgressInterval) {
    clearTimeout(document.monetization._wmProgressInterval)
  }
  
  document.monetization.state = 'stopped'
  document.monetization.dispatchEvent(new CustomEvent('monetizationstop', {
    detail: {
      requestId: document.monetization._requestId,
      paymentPointer: wmTag.content,
      finalized: true
    }
  }))
}

function isWm () {
  const pageUrl = new URL(window.location)
  return pageUrl.searchParams.get('wm') === 'true'
}

function start () {
  console.log('creating web monetization')
  initWm()
}

start()
