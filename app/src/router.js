import { createRouter, createWebHistory } from 'vue-router'
import Home from './pages/home.vue'
import Imagem from './pages/imagem.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/imagem/:id',
    name: 'imagem',
    component: Imagem
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
